## epson_g364_imu_driver (kinetic) - 0.0.2-0

The packages in the `epson_g364_imu_driver` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic epson_g364_imu_driver --edit` on `Wed, 30 May 2018 12:51:06 -0000`

The `epson_imu_driver` package was released.

Version of package(s) in repository `epson_g364_imu_driver`:
- upstream repository: git@bitbucket.org:castacks/epson_g364_imu_driver.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `0.0.2-0`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.3.1`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


# README #

### What is this repository for? ###

* Quick summary
This code provides interface between Epson IMU G364 and ROS.

It is based on the C driver released by Epson: http://vdc.epson.com/index.php?option=com_docman&task=cat_view&gid=373&Itemid=435

### How do I get use the driver? ###

roslaunch epson_imu_driver epson_g364.launch

This file contains parameters for configuring which data you want from the IMU, the update rate, etc. All parameters are described in the launch file.