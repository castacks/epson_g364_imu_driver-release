//==============================================================================
//
// 	main_screen.c - Epson IMU sensor test application
//                   - This program initializes the Epson IMU and 
//                     sends sensor output to console
//
//
//  THE SOFTWARE IS RELEASED INTO THE PUBLIC DOMAIN.
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
//  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, 
//  SECURITY, SATISFACTORY QUALITY, AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT
//  SHALL EPSON BE LIABLE FOR ANY LOSS, DAMAGE OR CLAIM, ARISING FROM OR IN CONNECTION
//  WITH THE SOFTWARE OR THE USE OF THE SOFTWARE.
//
//==============================================================================
#include <stdint.h>
#include <stdio.h>
#include <time.h>

#include "hcl.h"
#include "hcl_gpio.h"
#include "sensor_epsonCommon.h"


#include <termios.h> // Low-level functions for UART communication
#include "hcl_uart.h"

int comPort;
// Modify below as needed for hardware 
const char *IMUSERIAL = "/dev/ttyUSB0";
const int IMUBAUD = B460800;

// Determines the number of samples to readout before exiting the program
const unsigned int NUM_SAMPLES = 1000;
    
int main(int argc, char *argv[])
{
    int i;
    unsigned int sample = 0;
    signed short readData[SENSOR_READ_LEN];
    double ppSensorData[6];
    unsigned short chksum16_ver;

    // 1) Initialize the Seiko Epson HCL layer
    printf("\r\nInitializing HCL layer...");
    if (!seInit()){
        printf("\r\nError: could not initialize the Seiko Epson HCL layer. Exiting...\r\n");
        return -1;
    }
    printf("...done.\r\n");

    // 2) Initialize the GPIO interfaces, For GPIO control of pins SPI CS, RESET, DRDY
    printf("\r\nInitializing GPIO interface...");
    if (!gpioInit()){
        printf("\r\nError: could not initialize the GPIO layer. Exiting...\r\n");
        return -1;
    }
    printf("...done.");

    // 3) Initialize UART Interface
    printf("\r\nInitializing UART interface...");
    comPort = uartInit(IMUSERIAL, IMUBAUD);
    if(comPort == -1)
    {   
        printf("\r\nError: could not initialize UART interface. Exiting...\r\n");
        return -1;
    }
    printf("...done.");
   
    // 4) Power on sequence - force sensor to config mode, HW reset sensor
    //      Check for errors
    printf("\r\nChecking sensor NOT_READY status...");
    if(!sensorPowerOn())
    {
        printf("\r\nError: failed to power on Sensor. Exiting...\r\n");
	    uartRelease(comPort);
        return -1;
    }
    printf("...done.");
    
    // Initialize sensor with desired settings
    printf("\r\nInitializing Sensor...");
    if(!sensorInit())
    {
        printf("\r\nError: could not initialize Epson Sensor. Exiting...\r\n");
	    uartRelease(comPort);
        return -1;
    }
    else
    {
        printf("...Epson IMU initialized.");        
    }
    // Initialize text files for data logs
    const time_t date   = time(NULL);                   // Functions for obtaining and printing time and date
    struct tm tm        = *localtime(&date);
   
    // Epson IMU Data 
    printf("Date: ");
    printf("%s",ctime(&date));
    printf("\r\nEpson IMU\r\nSAMPLE,\tXGYRO,\tYGYRO,\tZGYRO,\tXACCL,\tYACCL,\tZACCL,\tCount");
    printf("\r\n...Epson IMU Logging.\r\n");
    sensorStart();

    while (1)
    {
        // For SPI interface, check if DRDY pin asserted
        // For UART interface, check if UART recv buffer contains a sensor sample packet
        if(sensorDataReady())
        {
#if defined G350 || defined V340 // 16-bit Sensor Output
            sensorDataReadBurstN(readData, SENSOR_READ_LEN);
            ppSensorDataRead16N(ppSensorData, readData, 2); // GyroX is located at index 2 of readData
            printf("\r\n%u,\t%+08f,\t%+08f,\t%+08f,\t%+08f,\t%+08f,\t%+08f,\t%5d", sample, ppSensorData[0], ppSensorData[1], ppSensorData[2], ppSensorData[3], ppSensorData[4], ppSensorData[5], (unsigned short)readData[9]);
#else // 32-bit Sensor Output
            sensorDataReadBurstN(readData, SENSOR_READ_LEN);
            unsigned short tmp16 = calChecksum16(readData, SENSOR_READ_LEN-1);
            ppSensorDataRead32N(ppSensorData, readData, 0); // GyroX is located at index 0 of readData

            printf("\r\n%u,\t%+08f,\t%+08f,\t%+08f,\t%+08f,\t%+08f,\t%+08f,\t%5d,\t0x%04hX,\t0x%04hX", sample, ppSensorData[0], ppSensorData[1], ppSensorData[2], ppSensorData[3], ppSensorData[4], ppSensorData[5], (unsigned short)readData[12],
            (unsigned short)readData[13], tmp16);
            if (tmp16 == (unsigned short)readData[SENSOR_READ_LEN-1])
                printf(", OK");
            else
                printf(", ERROR");
#endif
            sample++;
        }
        if (sample > (NUM_SAMPLES-1))
            break;
    }
    
    const time_t end = time(NULL);                    // Functions for obtaining and printing time and data
    tm  = *localtime(&end);
    printf("\r\nEnd: ");
    printf("%s", ctime(&end));

    sensorStop();
    sleep(1);
    uartRelease(comPort);
    gpioRelease();
    seRelease();
    printf("\r\n");
    
    printf("\r\nThe following arguments were passed to main(): ");
    for(i=1; i<argc; i++) printf("%s ", argv[i]);
    printf("\r\n");
    return 0;
}
