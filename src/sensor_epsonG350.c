//==============================================================================
//
//  sensor_epsonG350.c - Epson IMU sensor protocol specific code for G350 
//
//
//  THE SOFTWARE IS RELEASED INTO THE PUBLIC DOMAIN.
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
//  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, 
//  SECURITY, SATISFACTORY QUALITY, AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT
//  SHALL EPSON BE LIABLE FOR ANY LOSS, DAMAGE OR CLAIM, ARISING FROM OR IN CONNECTION
//  WITH THE SOFTWARE OR THE USE OF THE SOFTWARE.
//
//==============================================================================
#include "sensor_epsonCommon.h"
#include "hcl.h"
#include "hcl_gpio.h"

/*****************************************************************************
** Function name:       sensorInit
** Description:         Initialize the sensor hardware to desired settings
**                      Data Output Rate, Filter Setting, Burst Read Sensor Data
** Parameters:          None
** Return value:        1=success, 0=fail
*****************************************************************************/
int sensorInit(void)
{
    unsigned short rxData;
    unsigned int debug = FALSE;
    unsigned short retryCount = 3000;

    // Safety Measure, Incase Device is in Sampling Mode, goto Config Mode
    do
    {
        registerWriteByte(CMD_WINDOW0, ADDR_MODE_CTRL_HI, CMD_END_SAMPLING, debug);
        rxData = registerRead16(CMD_WINDOW0, ADDR_MODE_CTRL_LO, debug);
        seDelayMicroSecs(1000);
        retryCount--;
    } while((rxData & 0x0400) == 0x0000 && (retryCount != 0));

    if (retryCount == 0)
    {
        printf("\r\n...Error: Stuck in Sampling Mode.");
        return FALSE;
    }

    // POL_CTRL = Not Inverted
    registerWriteByte(CMD_WINDOW0, ADDR_SIG_CTRL_LO, 0x00, debug);

    // EXT=GPIO2, EXT_POL=Positive Logic, DRDY_ON=Data_Ready, DRDY_POL = Active Low
    registerWriteByte(CMD_WINDOW0, ADDR_MSC_CTRL_LO, CMD_DRDY_GPIO1, debug);

    // Set samples per second to 125
    registerWriteByte(CMD_WINDOW0, ADDR_SMPL_CTRL_HI, CMD_RATE125, debug);

    // Filter TAP = 16
    registerWriteByte(CMD_WINDOW0, ADDR_MODE_CTRL_LO, CMD_FLTAP16, debug);

    // AUTO_START=Disable, UART_AUTO=Enable (For UART Interface)
    registerWriteByte(CMD_WINDOW1, ADDR_UART_CTRL_LO, 0x01, debug);

    // Enable COUNT
    registerWriteByte(CMD_WINDOW0, ADDR_COUNT_CTRL_LO, 0x01, debug);

    return TRUE; /* success */
}


/*****************************************************************************
** Function name:       registerDump
** Description:         Read all registers for debug purpose
** Parameters:          None
** Return value:        None
*****************************************************************************/
void registerDump(void)
{
    unsigned int debug = TRUE;
    printf("\r\nRegister Dump:\r\n");
    registerRead16(0x00, 0x00, debug);
    registerRead16(0x00, 0x02, debug);
    registerRead16(0x00, 0x04, debug);
    printf("\r\n");
    registerRead16(0x00, 0x06, debug);
    registerRead16(0x00, 0x08, debug);
    registerRead16(0x00, 0x0A, debug);
    printf("\r\n");
    registerRead16(0x00, 0x0C, debug);
    registerRead16(0x00, 0x0E, debug);
    registerRead16(0x00, 0x10, debug);
    printf("\r\n");
    registerRead16(0x00, 0x12, debug);
    registerRead16(0x00, 0x32, debug);
    registerRead16(0x00, 0x34, debug);
    printf("\r\n");
    registerRead16(0x00, 0x36, debug);
    registerRead16(0x00, 0x38, debug);
    registerRead16(0x00, 0x3A, debug);
    printf("\r\n");
    registerRead16(0x00, 0x3C, debug);
    registerRead16(0x00, 0x3E, debug);
    registerRead16(0x00, 0x50, debug);
    printf("\r\n");
}
