//==============================================================================
//
//  sensor_epsonUart.c - Epson IMU sensor protocol UART specific code
//
//
//  THE SOFTWARE IS RELEASED INTO THE PUBLIC DOMAIN.
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
//  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, 
//  SECURITY, SATISFACTORY QUALITY, AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT
//  SHALL EPSON BE LIABLE FOR ANY LOSS, DAMAGE OR CLAIM, ARISING FROM OR IN CONNECTION
//  WITH THE SOFTWARE OR THE USE OF THE SOFTWARE.
//
//==============================================================================
#include "sensor_epsonCommon.h"
#include "hcl.h"
#include "hcl_gpio.h"
#include "hcl_uart.h"

// These are declared by the main() application for UART IF
extern const char *IMUSERIAL;   // COM port device name
extern int comPort;             // COM port handle
unsigned char rxBuffer[256];    // COM port receive buffer


// UART Interface Timing
// TWRITERATE/TREADRATE = 200us min @ 460800 BAUD, 1 command = 3 bytes = 3 * 22us = 66us
// TSTALL = 200us - 66us = 134us
#define EPSON_STALL                 134     // Microseconds

// UART commands are always terminated with 0x0D
const unsigned char UART_DELIMITER = 0x0D;  // Placed at the end of all UART cycles

/*****************************************************************************
** Function name:       sensorDataByteLength
** Description:         Configures the IMU based on the parameters in the Epson Options struct.
** Parameters:          options - The struct describing how the IMU should be configured.
** Return value:        1=success, 0=fail
*****************************************************************************/
unsigned int sensorDataByteLength(struct EpsonOptions options){
  unsigned int length = 0;
  if(options.flag_out)
    length += 2;
  if(options.temp_out){
    if(options.temp_bit)
      length += 4;
    else
      length += 2;
  }
  if(options.gyro_out){
    if(options.gyro_bit)
      length += 12;
    else
      length += 6;
  }
  if(options.accel_out){
    if(options.accel_bit)
      length += 12;
    else
      length += 6;
  }
  if(options.gpio_out)
    length += 2;
  if(options.count_out)
    length += 2;
  if(options.checksum_out)
    length += 2;
  
  length += 2; // for start and end byte
  
  return length;
}

/*****************************************************************************
** Function name:       sensorDataReady
** Description:         For UART interface check if comport recv buffer
**                          contains a burst of data
** Parameters:          None
** Return value:        1=success, 0=fail
*****************************************************************************/
int sensorDataReady(void)
{
    // Sensor data is ready when UART recv buffer contains atleast 1 sensor sample
    seDelayMS(1);
    int count;
    count = numBytesReadComPort(comPort);

    // G350/V340 One burst of sensor data: NDFLAG(2), Temp(2), Gx(2), Gy(2), Gz(2), Ax(2), Ay(2), Az(2), GPIO(2), Count(2), Header/Delimiter (2)
    // G352/G262/G354/G364/G320 One burst of sensor data: Gx(4), Gy(4), Gz(4), Ax(4), Ay(4), Az(4), Count(2), CHKSM16(2), Header/Delimiter(2)
    // BURST_LEN is defined in the specific IMU Model Header sensor_epsonXXXX.h
    if (count >= BURSTLEN)
        return TRUE;
    else
        return FALSE;
}

int sensorDataReadyOptions(struct EpsonOptions options){
  seDelayMS(1);
  int count = numBytesReadComPort(comPort);
  
  if(count >= sensorDataByteLength(options))
    return TRUE;
  return FALSE;
}

/*****************************************************************************
** Function name:       registerWriteByte
** Description:         Write Byte to Register = Set WIN_ID, Write Data 
**                      to Register
**                      NOTE: G350/V340 does not have WINDOW_ID function
**                            winNumber input parameter is ignored
** Parameters:          Window Number, Register Address, Register Write Byte, 
**                      Verbose Flag
** Return value:        None
*****************************************************************************/
void registerWriteByte(unsigned char winNumber, unsigned char regAddr, unsigned char regByte, unsigned int verbose)
{
    unsigned char txData[3];

#if !(defined G350 || defined V340)
    txData[0] = ADDR_WIN_CTRL|0x80;  //msb is 1b for register writes
    txData[1] = winNumber;
    txData[2] = UART_DELIMITER;
    writeComPort(comPort, txData, 3);
    EpsonStall();
#endif

    txData[0] = regAddr | 0x80; // msb is 1b for register writes
    txData[1] = regByte;
    txData[2] = UART_DELIMITER;
    writeComPort(comPort, txData, 3);
    EpsonStall();

    if (verbose)
    {
        printf("\r\nREG[0x%02X(W%01X)] < 0x%02X\t", regAddr, winNumber, regByte);
    }
}

/*****************************************************************************
** Function name:       registerRead16
** Description:         Read 16-bit from Register
**                      NOTE: G350/V340 does not have WINDOW_ID function
**                            winNumber input parameter is ignored
** Parameters:          Window Number, Register Address, Verbose Flag
** Return value:        Register Read Value 16-bit
*****************************************************************************/
unsigned short registerRead16(unsigned char winNumber, unsigned char regAddr, unsigned int verbose)
{
    unsigned char response[4] = {0};
    int size;
    unsigned char txData[3];

#if !(defined G350 || defined V340)
    txData[0] = ADDR_WIN_CTRL|0x80;
    txData[1] = winNumber;
    txData[2] = UART_DELIMITER;
    writeComPort(comPort, txData, 3);
    EpsonStall();
#endif

    txData[0] = regAddr & 0x7E; // msb is 0b for register reads & address must be even
    txData[1] = 0x00;
    txData[2] = UART_DELIMITER;
    writeComPort(comPort, txData, 3);
    //purgeComPort(comPort);//flush port - Commented out because seemed to have no effect
    EpsonStall();

    // Attempt to read 4 bytes from serial port
    // Validation check: Should be atleast 4 bytes, First byte should be Register Address,
    //                   Last byte should be delimiter
    size = readComPort(comPort, &response[0], 4);
    if ((size<4) || (response[0] != txData[0]) || (response[3] != UART_DELIMITER))
        printf("Returned less data or unexpected data from previous command.\n");
    EpsonStall();
    
    if (verbose)
    {
        printf("REG[0x%02X(W%01X)] > 0x%02X%02X\t", regAddr, winNumber, response[1], response[2]);
    }
    return (unsigned short)response[1]<<8|(unsigned short)response[2];
}


static unsigned char data[256];
#define START 0
#define DATA 1
#define END 2
static int state = START;
static int data_count = 0;


/*****************************************************************************
** Function name:       populateEpsonData
** Description:         Retrieves data from the IMU based on how it is configured.
** Parameters:          options - The struct describing how the IMU is configured.
**                      epson_data - The struct that is filled with data from the IMU.
** Return value:        none
** Notes:
******************************************************************************/
void populateEpsonData(struct EpsonOptions options, struct EpsonData* epson_data){
  
  int f1, f2;
  int t1, t2, t3, t4;
  int gx1, gx2, gx3, gx4;
  int gy1, gy2, gy3, gy4;
  int gz1, gz2, gz3, gz4;
  int ax1, ax2, ax3, ax4;
  int ay1, ay2, ay3, ay4;
  int az1, az2, az3, az4;
  int gp1, gp2;
  int c1, c2;
  
  if(options.flag_out)
    f1 = 0, f2 = 1;
  else
    f1 = 0, f2 = -1;

  if(options.temp_out)
    t1 = f2 + 1, t2 = t1 + 1, t3 = t2 + 1, t4 = t3 + 1;
  else
    t1 = t2 = t3 = t4 = f2;
  if(!options.temp_bit)
    t3 = t4 = t2;

  if(options.gyro_out)
    gx1 = t4 + 1, gx2 = gx1 + 1, gx3 = gx2 + 1, gx4 = gx3 + 1;
  else
    gx1 = gx2 = gx3 = gx4 = t4;
  if(!options.gyro_bit)
    gx3 = gx4 = gx2;

  if(options.gyro_out)
    gy1 = gx4 + 1, gy2 = gy1 + 1, gy3 = gy2 + 1, gy4 = gy3 + 1;
  else
    gy1 = gy2 = gy3 = gy4 = t4;
  if(!options.gyro_bit)
    gy3 = gy4 = gy2;

  if(options.gyro_out)
    gz1 = gy4 + 1, gz2 = gz1 + 1, gz3 = gz2 + 1, gz4 = gz3 + 1;
  else
    gz1 = gz2 = gz3 = gz4 = t4;
  if(!options.gyro_bit)
    gz3 = gz4 = gz2;

  if(options.accel_out)
    ax1 = gz4 + 1, ax2 = ax1 + 1, ax3 = ax2 + 1, ax4 = ax3 + 1;
  else
    ax1 = ax2 = ax3 = ax4 = gz4;
  if(!options.accel_bit)
    ax3 = ax4 = ax2;

  if(options.accel_out)
    ay1 = ax4 + 1, ay2 = ay1 + 1, ay3 = ay2 + 1, ay4 = ay3 + 1;
  else
    ay1 = ay2 = ay3 = ay4 = gz4;
  if(!options.accel_bit)
    ay3 = ay4 = ay2;

  if(options.accel_out)
    az1 = ay4 + 1, az2 = az1 + 1, az3 = az2 + 1, az4 = az3 + 1;
  else
    az1 = az2 = az3 = az4 = gz4;
  if(!options.accel_bit)
    az3 = az4 = az2;

  if(options.gpio_out)
    gp1 = az4 + 1, gp2 = gp1 + 1;
  else
    gp1 = gp2 = az4;
  
  if(options.count_out)
    c1 = gp2 + 1, c2 = c1 + 1;
  else
    c1 = c2 = gp2;
  
  if(options.temp_bit){
    int temp = (data[t1] << 8*3) + (data[t2] << 8*2) + (data[t3] << 8) + data[t4];
    epson_data->temperature = (temp - 172621824)*EPSON_TEMP_SF/65536 + 25;
  }
  else{
    short temp = (data[t1] << 8) + data[t2];
    epson_data->temperature = (temp - 2634)*EPSON_TEMP_SF + 25;
  }

  if(options.gyro_bit){
    int gyro_x = (data[gx1] << 8*3) + (data[gx2] << 8*2) + (data[gx3] << 8)+data[gx4];
    int gyro_y = (data[gy1] << 8*3) + (data[gy2] << 8*2) + (data[gy3] << 8)+data[gy4];
    int gyro_z = (data[gz1] << 8*3) + (data[gz2] << 8*2) + (data[gz3] << 8)+data[gz4];
    epson_data->gyro_x = (EPSON_GYRO_SF/65536)*3.14159/180.0 * gyro_x;
    epson_data->gyro_y = (EPSON_GYRO_SF/65536)*3.14159/180.0 * gyro_y;
    epson_data->gyro_z = (EPSON_GYRO_SF/65536)*3.14159/180.0 * gyro_z;
  }
  else{
    short gyro_x = (data[gx1] << 8) + data[gx2];
    short gyro_y = (data[gy1] << 8) + data[gy2];
    short gyro_z = (data[gz1] << 8) + data[gz2];
    epson_data->gyro_x = EPSON_GYRO_SF*3.14159/180.0 * gyro_x;
    epson_data->gyro_y = EPSON_GYRO_SF*3.14159/180.0 * gyro_y;
    epson_data->gyro_z = EPSON_GYRO_SF*3.14159/180.0 * gyro_z;
  }

  if(options.accel_bit){
    int accel_x = (data[ax1] << 8*3) + (data[ax2] << 8*2) +
      (data[ax3] << 8) + data[ax4];
    int accel_y = (data[ay1] << 8*3) + (data[ay2] << 8*2) +
      (data[ay3] << 8) + data[ay4];
    int accel_z = (data[az1] << 8*3) + (data[az2] << 8*2) +
      (data[az3] << 8) + data[az4];
    epson_data->accel_x = (EPSON_ACCL_SF/65536)*9.80665/1000.0 * accel_x;
    epson_data->accel_y = (EPSON_ACCL_SF/65536)*9.80665/1000.0 * accel_y;
    epson_data->accel_z = (EPSON_ACCL_SF/65536)*9.80665/1000.0 * accel_z;
  }
  else{
    short accel_x = (data[ax1] << 8) + (data[ax2]);
    short accel_y = (data[ay1] << 8) + (data[ay2]);
    short accel_z = (data[az1] << 8) + (data[az2]);
    epson_data->accel_x = (EPSON_ACCL_SF)*9.80665/1000.0 * accel_x;
    epson_data->accel_y = (EPSON_ACCL_SF)*9.80665/1000.0 * accel_y;
    epson_data->accel_z = (EPSON_ACCL_SF)*9.80665/1000.0 * accel_z;
  }


  int count = (data[c1] << 8) + data[c2];
  epson_data->count = count*EPSON_COUNT_SF;
}

int sensorDataReadBurstNOptions(struct EpsonOptions options, struct EpsonData* epson_data){
  int byte_length = sensorDataByteLength(options);
  int data_length = byte_length - 2;
  unsigned char byte;
  int i;
  //printf("length: %d\n", byte_length);
  while(readComPort(comPort, &byte, 1) > 0){
    //printf("state: %d, byte: 0x%02X\n", state, byte);
    switch(state){
    case START:
      if(byte == 0x80)
	state = DATA;
      break;
    case DATA:
      data[data_count] = byte;
      data_count++;
      if(data_count == byte_length - 2)
	state = END;
      break;
    case END:
      data_count = 0;
      state = START;
      if(byte == 0x0D){
	unsigned short checksum = 0;
	//for(i = 0; i < data_length; i++)
	//  printf("0x%02X ", data[i]);
	//printf("\n");
	for(i = 0; i < data_length-2; i += 2)
	  checksum += (data[i] << 8) + data[i+1];
	unsigned short epson_checksum = (data[data_length - 2] << 8) +
	  data[data_length - 1];
	
	//printf("actual: 0x%04X, expected: 0x%04X\n", checksum, epson_checksum);
	
	if(checksum == epson_checksum){
	  populateEpsonData(options, epson_data);
	  return TRUE;
	}
	else
	  printf("checksum failed\n");
      }
      return FALSE;
      break;
    }
  }
  return FALSE;
}

/*****************************************************************************
** Function name:       sensorDataReadBurstN
** Description:         Perform burst read to acquire sensor data
**                      NOTE: Not supported for SPI IF G350/V340
** Parameters:          pointer to signed short array, size of array
** Return value:        none
** Notes:
** 1. The burst packet consists of 16-bit data units.
**    For 32-bit sensor output, Total = GyroXYZ(4*3) + AccelXYZ(4*3) + Count(2) + Chksum(2) = 28 bytes = 14 words
**    For 16-bit sensor output, Total = ND_FLAGS(2) + Temp(2) + GyroXYZ(2*3) + AccelXYZ(2*3) + GPIO(2) + Count(2) = 20 bytes = 10 words
** 2. For SPI interface, call this function only after detecting DRDY is asserted.
**    For SPI interface, this function will send N+1 SPI commands to read N registers.
**    Maximum SPI clock is 1MHz for burst SPI reads.
** 3. For UART interface, call this function only after detecting UART recv buffer contains atleast 1 burst of data
** 4. For checksum16 validation, simply outputs error message if checksum mismatch detected (no other action performed)
**
**    To achieve high performance, we use this function to retrieve burst sensor data instead of calling registerRead16().
**
*****************************************************************************/
void sensorDataReadBurstN(signed short sensorReadData[], unsigned int readLen)
{
    // rxBuffer (in bytes) is 2 * SENSOR_READ_LEN(in 16-bits) + Burst Byte + Delimiter Byte
    int byteLength = (readLen*2) + 2;
    int i, j;
    int size;

    size = readComPort(comPort, &rxBuffer[0], byteLength);
    if(size<=0) printf("!");
    //for (i = 0; i < size; i++) printf("0x%02x,", rxBuffer[i]);
    //printf("\t%2i",size);

    // Convert bytes in rxBuffer to 16-bit words stored in sensorReadData array
    j = 0;
    for (i = 1; i < (byteLength-1); i=i+2)
    {
        //printf("%u %u ", i, j);
        unsigned short tmp  = rxBuffer[i];
        sensorReadData[j]   = (tmp << 8) + rxBuffer[i+1];
        j++;
    }
    
    // debug
    int k;
    for(k = 0; k < byteLength-1; k++)
      printf("%d: 0x%02x, ", k, rxBuffer[k]);
    printf("\n");
    
    // Bypass for V340 or G350 since it does not support checksum16 function
#if !(defined G350 || defined V340)
    unsigned short chksum16_calc = 0;
    for (i = 0; i < readLen-1; i++)
        chksum16_calc += (unsigned short)sensorReadData[i];
    
    /*printf("\r\n");
    for (i = 0; i < readLen; i++) printf(",\t0x%04hX", sensorReadData[i]);
    printf(",\t0x%04hX", chksum16_calc);
    printf(" %i %i", chksum16_calc, sensorReadData[readLen-1]);*/
    
    if (chksum16_calc != (unsigned short)sensorReadData[readLen-1])
       printf(" Checksum error detected!");
#endif
}

